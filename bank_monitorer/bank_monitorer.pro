QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets charts printsupport

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/slots_frameOptionsGraph_vueDet.cpp \
    src/slots_vueEns.cpp

HEADERS += \
    headers/check_list.h \
    headers/donnee.h \
    headers/graphics.h \
    headers/mainwindow.h \
    headers/user_settings.h

FORMS += \
    forms/mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
