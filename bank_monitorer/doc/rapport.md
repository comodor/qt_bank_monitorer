**BOUALI Karim - L3 S6 Informatique**

# Projet IHM - Outil de supervision pour une banque
*L3 S6 - Interfaces Homme-Machine – Projet 2021*

[TOC]

## Aperçu
### Mode jour
![aperçu du mode jour](screenshot/mode_jour.png)
### Mode nuit
![aperçu du mode nuit](screenshot/mode_nuit.png)


## Fonctionnalité
Dans cette section je détaillerai mes choix d'implémentation sur toutes les pages et les bugs présents.

### Points communs entre les pages 
* Sur toutes les pages j'ai fait le choix de garder le même menu qui contient un boutons permettant d'accéder aux différentes pages ainsi que la date et le logo de la banque. Le fais d'avoir le même menu dans les différentes page sert de repère, et permet de naviguer entre les pages. Le voici :
![menu  commun aux pages](screenshot/menu.png)
* En plus du grisement du bouton associé à la page courante dans le menu, on retrouvera en haut à droite de chaque page le nom de la page courante pour pouvoir encore une fois se repérer.
* J'ai également implémenté des raccourcis clavier (modifiable dans les réglages) pour pouvoir naviguer entre les différentes pages, activer/désactiver le mode nuit, zoomer sur des graphiques. En théorie le logiciel peux s'utiliser sans souris en naviguant entre les objets avec la touche tabulation et le click souris serait remplacé par la touche espace, et mes raccourcis implémentés permettrait la navigation entre les pages plus aisément.   
* On retrouvera également des info-bulles sur les différents objets du logiciel indiquant brièvement l'utilité de l'objet et l'action à effectuer pour l'utiliser. Pour certains objets dont l'action peut être remplacé par un raccourci clavier, l'info-bulle indiquera en plus le raccourci clavier qui remplace le click souris. Ce qui permettrait de faire découvrir l'existence des raccourcis, ou rappeler le raccourci associé à l'action (qui est aussi visible depuis la page de personnalisation des raccourcis).

### Page vue d'ensemble
![page vue d'ensemble](screenshot/vue_ensemble.png)
Sur cette page assez minimaliste on retrouve une représentation graphique des différents canaux de l'agence par défaut sur une période choisit par défaut et un type de donnée choisit comme type de donnée par défaut (ici pourcentage).
Au centre on retrouve bandeau de paramétrage des graphique. J'ai choisi d'intégrer les objets de modifications de paramètre (sélecteur de date, d'agence...) dans une phrase pour rendre la première approche au logiciel assez intuitive : Vue d'ensemble sur la période du ... au ... pour l'agence ... avec le format ...
Les paramètres sélectionnés de base sont les paramètres choisit comme paramètre par défaut dans les réglages. 
Sur la droite ont retrouvera un bouton imprimer, affichant une fenêtre de message demandant le graphique de quel canal imprimer puis viens une fenêtre informant le succès de l'opération :
(Il sera possible d'annuler l'opération avec un bouton annuler)
![fenêtre de message vue d'ensemble](screenshot/dialog_vue_ensemble.png)

*remarque: certains boutons dans la boite de dialogue sont surlignés en bleu je n'ai pas réussi à enlever la couleur, ni à changer l'ordre pour placer tous les boutons annuler de toutes les boites de dialogues à gauche pour conserver une certaine cohérence.*

*remarque: par manque de temps l'impression n'est pas opérationnel, la version la "plus" aboutie est celle si on choisit le canal bancaire*

Les graphiques de la vue d'ensemble affichent la somme totale des ventes des différentes familles des canaux sur la période donnée.
L'échelle des ordonnées change en fonction du type de donnée choisit : si c'est en pourcentage l'échelle va de 0 à 100, si c'est en volume de 0 à 1000 et si recette de 0 à 10000.
J'ai volontairement choisis des borne supérieur max fixe pour conserver les mêmes proportions pour les trois graphiques et qu'on puisse les comparer entre eux visuellement. 

Lors du survol des graphiques le curseur devient une loupe avec un "+" pour indiquer que pour zoomer sur le graphique il faut clicker sur le graphique. Ainsi si on click sur le graphique du canal bancaire on se retrouve avec cette page :
![page graphique zoomé](screenshot/graphique_zoom.png)
Cette page est assez minimale pour laisser le maximum de place au graphique (utile pour les petits écrans). On retrouvera également une loupe avec "-" au survol du graphique qui fera doublon au bouton retour en haut à gauche.

### Page vue détaillée
![page vue détaillée](screenshot/vue_detaillee.png)
Sur cette page on retrouvera une vue détaillée des ventes. On pourra ainsi comparer les ventes de différents canaux, familles, produits, banquiers et agences. Et ceci sur une période donnée avec un affichage par semaine. 
Par soucis de lisibilité, si la période donnée est inférieur ou égale à 6 semaines les ventes seront détaillées sur les différentes semaines de la période sinon on aura juste la somme des ventes sur toute la période, dans ce cas là, une fenêtre apparaitra pour informer l'utilisateur. Cette fenêtre sera non modale pour que l'utilisateur puisse continuer à manipuler l'interface et se fermera toute seule au bout de quelques secondes.

Les éléments que l'on souhaite affichés doivent être sélectionnés dans le menu à onglets en bas à gauche. Il y a 3 onglets : Canaux, Familles et Versions, pour pouvoir comparer les canaux de la banque entre eux, les familles entre eux... 
Etant donné qu'il n'es pas possible de comparer différentes catégories entre elles (des canaux et des familles par exemple) il n'est pas possible d'accéder à un autre onglet si un élément d'un onglet est coché, les autres onglets sont donc "disable" et si l'utilisateur click dessus une fenêtre avec un message apparait pour l'informer.
Un bouton "Tout décocher" est également présent pour décocher tous les éléments de l'onglet courant et pouvoir accéder à un autre onglet plus rapidement. 
On pourra aussi retrouver un scrollbar si la liste d'éléments est trop longue par rapport à la taille de la fenêtre.
*remarque: Dans mes wireframes j'avais mis une barre de recherche pour retrouver un éléments plus rapidement, étant donné le peu de produit et de la complexité j'ai trouvé ca inutile. Ca pourrait être une fonctionnalité à ajouter par la suite dans le cas de l'évolution de la banque*

Les paramètres d'affichage des ventes sont dans la barre au dessus du graphique comme sur la vue d'ensemble à la différences qu'on peux sélectionner plusieurs agences :
![combobox d'agences dans la vue détaillée](screenshot/combobox_agences.png)
Si toutes les agences sont sélectionnées, on n'affiche pas la liste des agences mais le message "Toute les agences" par soucis de lisibilité et informatif pour savoir que toutes les agences sont sélectionnées.
L'affichage pour les banquiers est le même, cependant dans sa liste évolue en fonction des agences sélectionnées. On ne pourra choisir que des banquiers d'agences cochées.
Pour l'affichage des banquiers sur le graphique si tous les banquiers d'une agence sont cochés alors on sur le graphique on représentera les ventes de l'agence entière mais si c'est seulement que certains banquiers alors l'affichage sera divisé pour chaque banquiers. 

*remarque: par manque de temps je n'ai pas implémenté le fait de pouvoir sélectionner au minimum une agence ou un banquier sinon il n'y a rien à afficher*

*remarque: le bouton imprimer et détails ne sont pas fonctionnels, j'ai détaillée leur fonctionnement dans mes [wireframes](wireframes_LaBanque.pdf).*

### Page de réglage
![page de réglage](screenshot/reglage.png)
Cette page est assez minimaliste, elle permet seulement de changer les paramètres par défaut comme l'agence, le type de donnée...
Il n'y a pas grand chose à commenter, mis à part le fait que le bouton "appliquer les changements" est disable et ne devient clickable que si une modification a été effectuée. Et à l'appui du bouton une fenêtre apparait pour nous demander de confirmer nos modifications.  

### Page de customisation
![page de customisation](screenshot/customisation.png)
Cette page est assez similaire visuellement à celle des réglages, elle permet de customiser l'interface du logiciel avec l'activation du mode nuit (mode nuit demandé par les utilisateurs finaux) et de modifier les raccourcis claviers.
Le mode nuit si il est activé sera actif de base même si l'on ferme le logiciel et qu'on le relance. D'ailleurs c'est aussi le cas pour les raccourcis clavier et les choix de paramètres par défaut comme l'agence par exemple.


## Wireframe et fonctionnalités absentes
Mes wireframes sont accessibles [ici](wireframes_LaBanque.pdf).

### Page vue d'ensemble
Les éléments présents sur ma wireframe pour la page vue d'ensemble mais absent de mon rendu sont :
* Le bloc d'information avec l'agence de la semaine, le banquier de la semaine... J'avais trouvé ca intéressant et pouvait engendrer une compétition entre les agences. Et pensais rendre le module supprimable si ce n'étais pas aux goûts des utilisateurs finaux, ce qui pourrait laisser plus de place au module avec les courbes sur sa gauche.   
* Le graphiques avec les courbes des ventes des canaux de l'agence par défaut, et il aurait été possible de choisir la période dans les réglages mais aussi la catégories d'éléments à afficher (canaux, familles, produits).
* Un bouton permettant de changer le type de graphique. Actuellement on ne peut voir des graphiques avec des bâtonnets, ce bouton aurait permis de switcher avec des camemberts.   

### Page vue détaillée
Les éléments présents sur ma wireframe pour la page vue d'ensemble mais absent de mon rendu sont :
* Une barre de recherche dans le menu d'onglets permettant de sélectionner des éléments à afficher.
* Le trie des éléments dans un "tree widget" qui permet une meilleure lisibilité. Mais étant donné que je n'ai également pas implémenté le widget affichant tous les éléments sélectionnés, j'ai préféré l'affichage actuel car avec un tree widget certains éléments sont cachés et si il n'es pas précisé tous les éléments cochés ca pourrait poser des problèmes de visualisation pour l'utilisateur avec des éléments cochés mais caché.
* L'implémentation du bouton détails qui switcherai entre l'affichage par graphique et un affichage avec un tableau avec la liste des transactions des éléments sélectionnés sur la période choisit. Qui aurait permis de visualiser les détails de chaque transaction : le montant, le numéro anonyme d'un client (étant donné que c'est une donnée sensible on n'afficherai pas son nom), le banquier à l'auteur de la transaction...
* Et enfin le bouton imprimer permettant d'imprimer le graphique ou le tableau de transaction en fonction de la vue actuelle.


## Difficultés rencontrées
Etant donné la contrainte de temps et le temps passé à se familiariser avec l'outil Qt j'ai fais le choix de négliger un peu le back et de ne pas trop optimisé mon code et respecter le paradigme objet pour essayer de me rapprocher au maximum de ma wireframe et de mettre en œuvre les connaissances acquises durant les cours de la matière sur l'expérience utilisateur.
