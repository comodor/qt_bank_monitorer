#include "../headers/mainwindow.h"
#include "../ui_mainwindow.h"
#include "../headers/graphics.h"
#include "../headers/donnee.h"
#include "../headers/user_settings.h"
#include <ctime>
#include <QPrinter>
#include <QShortcut>
#include "../headers/check_list.h"



void MainWindow::print_pdf(QString canal)
{
    if (canal == "Bancaire")
        print_graph(ui->graphicsView, get_info_print(ui->comboBox_agence, ui->dateEdit_debut, ui->dateEdit_fin,
                                                      ui->comboBox_donnee, {"Bancaire"}, {"TODO"}, {"TODO"}));
    if (canal == "Assurance")
        print_graph(ui->graphicsView, get_info_print(ui->comboBox_agence, ui->dateEdit_debut, ui->dateEdit_fin,
                                                      ui->comboBox_donnee, {"Assurance"}, {"TODO"}, {"TODO"}));
    if (canal == "Boursier")
        print_graph(ui->graphicsView, get_info_print(ui->comboBox_agence, ui->dateEdit_debut, ui->dateEdit_fin,
                                                      ui->comboBox_donnee, {"Boursier"}, {"TODO"}, {"TODO"}));

    QMessageBox msgBox;
    QString path = m_last_path_pdf_create + m_last_pdf_create;
    QString msg =
            "Le graphique a été sauvegardé sous le nom : "
            + m_last_pdf_create + "\n"
            + "chemin vers le fichier : " + path
            + "\n\nVous pouvez modifier la destination des documents dans les reglages.";
    msgBox.setText(msg);
    msgBox.setWindowTitle("Impression réussie");

    QAbstractButton *reglage =
            msgBox.addButton(tr("Modifier la destination"), QMessageBox::NoRole);
    (void)reglage;
    QAbstractButton *voir =
          msgBox.addButton(tr("Ouvrir le document"), QMessageBox::ActionRole);
    QAbstractButton *ok =
          msgBox.addButton(tr("Ok"), QMessageBox::NoRole);
    (void)ok;

    msgBox.exec();
    if (msgBox.clickedButton() == voir)
        QDesktopServices::openUrl(QUrl::fromLocalFile(path));
}




void MainWindow::slot_msgChoixPrintEns() {
    QMessageBox choix;
    choix.setText("Veuillez choisir un graphique à enregistrer");
    choix.setWindowTitle("Choix du graphique");
    QAbstractButton *pdf1 =
          choix.addButton(tr("canal bancaire"), QMessageBox::ActionRole);
    QAbstractButton *pdf2 =
          choix.addButton(tr("canal assurance"), QMessageBox::ActionRole);
    QAbstractButton *pdf3 =
          choix.addButton(tr("canal boursier"), QMessageBox::ActionRole);
    QAbstractButton *annuler =
            choix.addButton(tr("annuler"), QMessageBox::NoRole);

    choix.exec();
    if (choix.clickedButton() == pdf1)
        print_pdf("Bancaire");
    if (choix.clickedButton() == pdf2)
        print_pdf("Assurance");
    if (choix.clickedButton() == pdf3)
        print_pdf("Boursier");

}









void MainWindow::edit_user_settings_defaut() {

    QMessageBox choix;
    choix.setText("Appliquer les changements ?");
    choix.setWindowTitle("Confirmation");
    QAbstractButton *ok =
          choix.addButton(tr("Ok"), QMessageBox::ActionRole);
    QAbstractButton *annuler =
          choix.addButton(tr("Annuler"), QMessageBox::NoRole);

    choix.exec();

    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());


    if (buttonSender->objectName() == "pushButton_appliquer_custom") {
        if (choix.clickedButton() == ok) {
            set_default_keys(ui->keySequenceEdit_nuit_custom, ui->keySequenceEdit_vueEns_custom, ui->keySequenceEdit_vueDet_custom,
                             ui->keySequenceEdit_custom_custom, ui->keySequenceEdit_reglage_custom, ui->keySequenceEdit_retour_custom,
                             ui->keySequenceEdit_Z1_custom, ui->keySequenceEdit_Z2_custom, ui->keySequenceEdit_Z3_custom,
                             ui->keySequenceEdit_ZD_custom);
            m_keys[0]->setKey(tr(get_default_key(6).c_str()));
            m_keys[1]->setKey(tr(get_default_key(7).c_str()));
            m_keys[2]->setKey(tr(get_default_key(8).c_str()));
            m_keys[3]->setKey(tr(get_default_key(9).c_str()));
            m_keys[4]->setKey(tr(get_default_key(10).c_str()));
            m_keys[5]->setKey(tr(get_default_key(11).c_str()));
            ui->pushButton_appliquer_custom->setEnabled(false);
        }
    }

    if (buttonSender->objectName() == "pushButton_appliquer_reglage") {
        if (choix.clickedButton() == ok) {
            set_default_agence(ui->comboBox_choixAgence_reglage);
            set_default_date_debut(ui->spinBox_choixPeriode_reglage);
            set_default_type_donnee(ui->comboBox_choixDonnee_reglage);
            set_default_devise(ui->radioButton_choixEuros_reglage, ui->radioButton_choixDollars_reglage, ui->radioButton_choixLivres_reglage);
            setup_reglage_defaut();
            ui->pushButton_appliquer_reglage->setEnabled(false);
        }
     }

}

void MainWindow::edit_gbar_det() {

    QChart* chart5 = create_gbar_det(get_data_det(),
                                     ui->comboBox_agences_vueDet, ui->comboBox_banquiers_vueDet,
                                     ui->dateEdit_debut_vueDet, ui->dateEdit_fin_vueDet);
    ui->graphicsView_vueDet->chart()->removeAllSeries();
    ui->graphicsView_vueDet->setChart(chart5);


    ui->pushButton_appliquer_vueDet->setEnabled(false);

}


void MainWindow::setup() {
    // affichage de la date
    QDate date;
    ui->label_date->setText(date.currentDate().toString());
    ui->label_date->setAlignment(Qt::AlignCenter);
    ui->label_date_custom->setText(date.currentDate().toString());
    ui->label_date_custom->setAlignment(Qt::AlignCenter);
    ui->label_date_reglage->setText(date.currentDate().toString());
    ui->label_date_reglage->setAlignment(Qt::AlignCenter);

    // application du type de donnée par defaut
    if (get_default_type_donnee() == RECETTE)
        ui->comboBox_donnee->setCurrentText("Recette");
    else if (get_default_type_donnee() == POURCENTAGE)
        ui->comboBox_donnee->setCurrentText("Pourcentage");
    else
        ui->comboBox_donnee->setCurrentText("Volume");
    if (get_default_type_donnee() == RECETTE)
        ui->comboBox_choixDonnee_reglage->setCurrentText("Recette");
    else if (get_default_type_donnee() == POURCENTAGE)
        ui->comboBox_choixDonnee_reglage->setCurrentText("Pourcentage");
    else
        ui->comboBox_choixDonnee_reglage->setCurrentText("Volume");


    // application de l'agence par defaut
    ui->comboBox_agence->setCurrentText(get_default_agence());
    ui->comboBox_choixAgence_reglage->setCurrentText(get_default_agence());

    // application de la periode par defaut
    ui->dateEdit_fin->setDate(QDate::currentDate());
    ui->dateEdit_debut->setDate(QDate::currentDate().addDays(- (7*get_default_date_debut())));
    ui->dateEdit_fin_vueDet->setDate(QDate::currentDate());
    ui->dateEdit_debut_vueDet->setDate(QDate::currentDate().addDays(- (7*get_default_date_debut())));


    ui->spinBox_choixPeriode_reglage->setValue(get_default_date_debut());

    // application de la devise par defaut
    if (get_default_devise() == "euros")
        ui->radioButton_choixEuros_reglage->setChecked(true);
    else if (get_default_devise() == "dollars")
        ui->radioButton_choixDollars_reglage->setChecked(true);
    else
        ui->radioButton_choixLivres_reglage->setChecked(true);

    // application des raccourcis par defaut
     ui->keySequenceEdit_retour_custom->setKeySequence(tr(get_default_key(11).c_str()));
     ui->keySequenceEdit_custom_custom->setKeySequence(tr(get_default_key(9).c_str()));
     ui->keySequenceEdit_nuit_custom->setKeySequence(tr(get_default_key(6).c_str()));
     ui->keySequenceEdit_vueEns_custom->setKeySequence(tr(get_default_key(7).c_str()));
     ui->keySequenceEdit_vueDet_custom->setKeySequence(tr(get_default_key(8).c_str()));
     ui->keySequenceEdit_reglage_custom->setKeySequence(tr(get_default_key(10).c_str()));
     ui->keySequenceEdit_Z1_custom->setKeySequence(tr(get_default_key(12).c_str()));
     ui->keySequenceEdit_Z2_custom->setKeySequence(tr(get_default_key(13).c_str()));
     ui->keySequenceEdit_Z3_custom->setKeySequence(tr(get_default_key(14).c_str()));
     ui->keySequenceEdit_ZD_custom->setKeySequence(tr(get_default_key(15).c_str()));

     // application mode nuit defaut
     if (get_default_mode_nuit())
         ui->checkBox_nuit_custom->setChecked(true);


    // affichage des graphique du main
    QChart* chart = create_gbar(get_familles_bancaire(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView->setChart(chart);
    QChart* chart2 = create_gbar(get_familles_assurance(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_2->setChart(chart2);
    QChart* chart3 = create_gbar(get_familles_boursier(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_3->setChart(chart3);
    QChart* chart4 = create_gbar(get_familles_boursier(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_zoom->setChart(chart4);







    // grisement des boutons du main
    ui->pushButton_vueEns->setEnabled(false);

    // grisement des bouton custom
    ui->pushButton_custom_custom->setEnabled(false);
    ui->pushButton_appliquer_custom->setEnabled(false);

    // grisement des bouton reglage
    ui->pushButton_reglage_reglage->setEnabled(false);
    ui->pushButton_appliquer_reglage->setEnabled(false);

    // grisement des bouttons vueDet
    ui->pushButton_vueDet_vueDet->setEnabled(false);
    ui->pushButton_appliquer_vueDet->setEnabled(false);

    // infos bulles
    ui->graphicsView->setToolTip("Clickez pour zoomer ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(12)) + "\"");
    ui->graphicsView_2->setToolTip("Clickez pour zoomer ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(13)) + "\"");
    ui->graphicsView_3->setToolTip("Clickez pour zoomer ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(14)) + "\"");

    ui->pushButton_vueEns->setToolTip("La page courante est déja la page \"Vue d'ensemble\"");
    ui->pushButton_vueDet->setToolTip("Clickez pour acceder à la page \"Vue détaillée\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(8)) + "\"");
    ui->pushButton_custom->setToolTip("Clickez pour acceder à la page \"Customisation\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(9)) + "\"");
    ui->pushButton_reglage->setToolTip("Clickez pour acceder à la page \"Reglage\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(10)) + "\"");
    ui->print_button->setToolTip("Clickez pour imprimer un graphique");
    ui->dateEdit_debut->setToolTip("Clickez pour selectionner la date de debut de la période");
    ui->dateEdit_fin->setToolTip("Clickez pour selectionner la date de fin de la période");
    ui->comboBox_agence->setToolTip("Clickez pour selectionner l'agence à afficher");
    ui->comboBox_donnee->setToolTip("Clickez pour selectionner le type de donnée");

    if (ui->pushButton_appliquer_custom->isEnabled())
        ui->pushButton_appliquer_custom->setToolTip("Vous devez changer au moins un parametre pour appliquer les changements");
    else
        ui->pushButton_appliquer_custom->setToolTip("Appliquer les changements aux parametres par defaut");

    ui->pushButton_vueEns_custom->setToolTip("Clickez pour acceder à la page \"Vue d'ensemble\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(7)) + "\"");
    ui->pushButton_vueDet_custom->setToolTip("Clickez pour acceder à la page \"Vue détaillée\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(8)) + "\"");
    ui->pushButton_custom_custom->setToolTip("La page courante est déja la page \"Customisation\"");
    ui->pushButton_reglage_custom->setToolTip("Clickez pour acceder à la page \"Reglage\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(10)) + "\"");
    ui->keySequenceEdit_Z1_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_Z2_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_Z3_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_ZD_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_nuit_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_vueEns_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_vueDet_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_reglage_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_retour_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");
    ui->keySequenceEdit_custom_custom->setToolTip("Clickez puis entrez la combinaison clavier pour créer un raccourci");

    if (ui->checkBox_nuit_custom->isChecked())
        ui->checkBox_nuit_custom->setToolTip("Decochez la case pour desactiver le mode nuit");
    else
        ui->checkBox_nuit_custom->setToolTip("Cochez la case pour activer le mode nuit");


    if (ui->pushButton_appliquer_reglage->isEnabled())
        ui->pushButton_appliquer_reglage->setToolTip("Vous devez changer au moins un parametre pour appliquer les changements");
    else
        ui->pushButton_appliquer_reglage->setToolTip("Appliquer les changements aux parametres par defaut");

    ui->pushButton_vueEns_reglage->setToolTip("Clickez pour acceder à la page \"Vue d'ensemble\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(7)) + "\"");
    ui->pushButton_vueDet_reglage->setToolTip("Clickez pour acceder à la page \"Vue détaillée\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(8)) + "\"");
    ui->pushButton_custom_reglage->setToolTip("Clickez pour acceder à la page \"Customisation\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(8)) + "\"");
    ui->pushButton_reglage_reglage->setToolTip("La page courante est déja la page \"Reglage\"");
    ui->comboBox_choixAgence_reglage->setToolTip("Clickez pour selectionner l'agence par defaut");
    ui->comboBox_choixDonnee_reglage->setToolTip("Clickez pour selectionner le type de donnée par defaut pour les graphiques");
    ui->spinBox_choixPeriode_reglage->setToolTip("Cliquez pour selectionner la durée de la periode par defaut pour les graphiques");
    if (ui->radioButton_choixDollars_reglage->isChecked())
        ui->radioButton_choixDollars_reglage->setToolTip("La devise Dollar est deja selectionnée comme devise par defaut");
    else
        ui->radioButton_choixDollars_reglage->setToolTip("Cliquez pour mettre le Dollar comme devise par defaut");
    if (ui->radioButton_choixEuros_reglage->isChecked())
        ui->radioButton_choixEuros_reglage->setToolTip("La devise Euro est deja selectionnée comme devise par defaut");
    else
        ui->radioButton_choixEuros_reglage->setToolTip("Cliquez pour mettre le Euro comme devise par defaut");
    if (ui->radioButton_choixLivres_reglage->isChecked())
        ui->radioButton_choixLivres_reglage->setToolTip("La devise Livre est deja selectionnée comme devise par defaut");
    else
        ui->radioButton_choixLivres_reglage->setToolTip("Cliquez pour mettre le Livre comme devise par defaut");

    ui->graphicsView_zoom->setToolTip("Cliquez pour revenir à la page \"Vue d'ensemble\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(11)) + "\"");
    ui->pushButton_retour_zoom->setToolTip("Cliquez pour revenir à la page \"Vue d'ensemble\" ou avec le raccourci clavier \"" + QString::fromStdString(get_default_key(11)) + "\"");


    // curseur
    ui->graphicsView->setCursor(QCursor(QPixmap("../bank_monitorer/ressources/zoom.ico").scaled(30, 30), 0, 0));
    ui->graphicsView_2->setCursor(QCursor(QPixmap("../bank_monitorer/ressources/zoom.ico").scaled(30, 30), 0, 0));
    ui->graphicsView_3->setCursor(QCursor(QPixmap("../bank_monitorer/ressources/zoom.ico").scaled(30, 30), 0, 0));
    ui->graphicsView_zoom->setCursor(QCursor(QPixmap("../bank_monitorer/ressources/dezoom.ico").scaled(30, 30), 0, 0));










    // selections des elements par defaut dans vueDet
    if (get_default_type_donnee() == RECETTE) {
        ui->comboBox_donnee_vueDet->setCurrentText("Recette");
    }
    else if (get_default_type_donnee() == POURCENTAGE) {
        ui->comboBox_donnee_vueDet->setCurrentText("Pourcentage");
    }
    else {
        ui->comboBox_donnee_vueDet->setCurrentText("Volume");
    }


    if (get_default_agence() == "Agence A") {
        ui->comboBox_agences_vueDet->addCheckItem("Agence A", QVariant::TextFormat, Qt::CheckState::Checked);
        ui->comboBox_banquiers_vueDet->addCheckItem("Mr. A1", QVariant::TextFormat, Qt::CheckState::Checked);
        ui->comboBox_banquiers_vueDet->addCheckItem("Mr. A2", QVariant::TextFormat, Qt::CheckState::Checked);
    }
    else
        ui->comboBox_agences_vueDet->addCheckItem("Agence A", QVariant::TextFormat, Qt::CheckState::Unchecked);
    if (get_default_agence() == "Agence B") {
        ui->comboBox_agences_vueDet->addCheckItem("Agence B", QVariant::TextFormat, Qt::CheckState::Checked);
        ui->comboBox_banquiers_vueDet->addCheckItem("Mr. B1", QVariant::TextFormat, Qt::CheckState::Checked);
        ui->comboBox_banquiers_vueDet->addCheckItem("Mr. B2", QVariant::TextFormat, Qt::CheckState::Checked);
    }
    else
        ui->comboBox_agences_vueDet->addCheckItem("Agence B", QVariant::TextFormat, Qt::CheckState::Unchecked);
    if (get_default_agence() == "Agence C") {
        ui->comboBox_agences_vueDet->addCheckItem("Agence C", QVariant::TextFormat, Qt::CheckState::Checked);
        ui->comboBox_banquiers_vueDet->addCheckItem("Mr. C1", QVariant::TextFormat, Qt::CheckState::Checked);
        ui->comboBox_banquiers_vueDet->addCheckItem("Mr. C2", QVariant::TextFormat, Qt::CheckState::Checked);
    }
    else
        ui->comboBox_agences_vueDet->addCheckItem("Agence C", QVariant::TextFormat, Qt::CheckState::Unchecked);



    ui->checkBox_canalBancaire_vueDet->setChecked(true);
    ui->checkBox_canalBoursier_vueDet->setChecked(true);

    ui->tabWidget_options_vueDet->setCurrentIndex(0);


    QChart* chart5 = create_gbar_det(get_data_det(),
                                     ui->comboBox_agences_vueDet, ui->comboBox_banquiers_vueDet,
                                     ui->dateEdit_debut_vueDet, ui->dateEdit_fin_vueDet);
    ui->graphicsView_vueDet->setChart(chart5);




    // raccourcis
    QShortcut *keyNuit= new QShortcut(tr(get_default_key(6).c_str()), this);
    QShortcut *keyVueEns= new QShortcut(tr(get_default_key(7).c_str()), this);
    QShortcut *keyVueDet= new QShortcut(tr(get_default_key(8).c_str()), this);
    QShortcut *keyCustom= new QShortcut(tr(get_default_key(9).c_str()), this);
    QShortcut *keyReglage= new QShortcut(tr(get_default_key(10).c_str()), this);
    QShortcut *keyRetour= new QShortcut(tr(get_default_key(11).c_str()), this);

    m_keys.push_back(keyNuit);
    m_keys.push_back(keyVueEns);
    m_keys.push_back(keyVueDet);
    m_keys.push_back(keyCustom);
    m_keys.push_back(keyReglage);
    m_keys.push_back(keyRetour);

    QObject::connect(keyNuit, SIGNAL(activated()), this, SLOT(raccourcis()));
    QObject::connect(keyVueEns, SIGNAL(activated()), this, SLOT(raccourcis()));
    QObject::connect(keyVueDet, SIGNAL(activated()), this, SLOT(raccourcis()));
    QObject::connect(keyCustom, SIGNAL(activated()), this, SLOT(raccourcis()));
    QObject::connect(keyReglage, SIGNAL(activated()), this, SLOT(raccourcis()));
    QObject::connect(keyRetour, SIGNAL(activated()), this, SLOT(raccourcis()));

    //application mode nuit
    applyTheme();



}

void MainWindow::raccourcis() {
    QShortcut* key = qobject_cast<QShortcut*>(sender());

    if (!key->key().toString().compare(QString::fromStdString(get_default_key(6)))) {
        if (ui->checkBox_nuit_custom->isChecked())
            ui->checkBox_nuit_custom->setChecked(false);
        else
            ui->checkBox_nuit_custom->setChecked(true);
        applyTheme();
    }

    if (!key->key().toString().compare(QString::fromStdString(get_default_key(7))))
        ui->stackedWidget->setCurrentIndex(VUE_ENS);

    if (!key->key().toString().compare(QString::fromStdString(get_default_key(8))))
        ui->stackedWidget->setCurrentIndex(VUE_DET);

    if (!key->key().toString().compare(QString::fromStdString(get_default_key(9))))
        ui->stackedWidget->setCurrentIndex(CUSTOM);

    if (!key->key().toString().compare(QString::fromStdString(get_default_key(10))))
        ui->stackedWidget->setCurrentIndex(REGLAGE);

    if (!key->key().toString().compare(QString::fromStdString(get_default_key(11))))
        ui->stackedWidget->setCurrentIndex(VUE_ENS);
}


void MainWindow::setup_reglage_defaut() {
    // application du type de donnée par defaut
    if (get_default_type_donnee() == RECETTE) {
        ui->comboBox_donnee->setCurrentText("Recette");
        ui->comboBox_choixDonnee_reglage->setCurrentText("Recette");
    }
    else if (get_default_type_donnee() == POURCENTAGE) {
        ui->comboBox_donnee->setCurrentText("Pourcentage");
        ui->comboBox_choixDonnee_reglage->setCurrentText("Pourcentage");
    }
    else {
        ui->comboBox_donnee->setCurrentText("Volume");
        ui->comboBox_choixDonnee_reglage->setCurrentText("Volume");
    }

    // application de l'agence par defaut
    ui->comboBox_agence->setCurrentText(get_default_agence());
    ui->comboBox_choixAgence_reglage->setCurrentText(get_default_agence());


    // application de la periode par defaut
    ui->dateEdit_debut->setDate(QDate::currentDate().addDays(- (7*get_default_date_debut())));





    edit_gbar();
}


void MainWindow::enable_btnAppliquer() {
    if (ui->stackedWidget->currentIndex() == REGLAGE)
        if (!ui->pushButton_appliquer_reglage->isEnabled())
            ui->pushButton_appliquer_reglage->setEnabled(true);
    if (ui->stackedWidget->currentIndex() == CUSTOM)
        if (!ui->pushButton_appliquer_custom->isEnabled())
            ui->pushButton_appliquer_custom->setEnabled(true);
    if (ui->stackedWidget->currentIndex() == VUE_DET)
        if (!ui->pushButton_appliquer_vueDet->isEnabled())
            ui->pushButton_appliquer_vueDet->setEnabled(true);
}

void MainWindow::enable_btnAppliquer_2(int i) {
    qDebug() << i;
    if (ui->stackedWidget->currentIndex() == REGLAGE)
        if (!ui->pushButton_appliquer_reglage->isEnabled())
            ui->pushButton_appliquer_reglage->setEnabled(true);
    if (ui->stackedWidget->currentIndex() == CUSTOM)
        if (!ui->pushButton_appliquer_custom->isEnabled())
            ui->pushButton_appliquer_custom->setEnabled(true);
    if (ui->stackedWidget->currentIndex() == VUE_DET)
        if (!ui->pushButton_appliquer_vueDet->isEnabled())
            ui->pushButton_appliquer_vueDet->setEnabled(true);
}



void MainWindow::applyTheme()
{

    set_default_mode_nuit(ui->checkBox_nuit_custom);

    if (get_default_mode_nuit()) {
        QFile file("../bank_monitorer/ressources/Mode_nuit.qss");
        file.open(QFile::ReadOnly);
        QString styleSheet = QString::fromLatin1(file.readAll());

        qApp->setStyleSheet(styleSheet);

        const auto areas = ui->stackedWidget->findChildren<QScrollArea*>();
        for(auto&& area : areas)
            area->setStyleSheet("background-color: #3d3d3d");


        QBrush qb;
        qb.setColor(Qt::black);

        const auto graphs = ui->stackedWidget->findChildren<QChartView*>();
        for(auto&& graph : graphs) {
            graph->setStyleSheet("background-color: #3d3d3d;" "border-radius: 5px;");
            graph->chart()->setBackgroundBrush((const QBrush)qb);
            graph->chart()->axisY()->setLabelsColor(Qt::white);
            graph->chart()->legend()->setLabelColor(Qt::white);
        }

    ui->graphicsView_vueDet->chart()->axisX()->setLabelsColor(Qt::white);


    }

    else {
        qApp->setStyleSheet(styleSheet());

        const auto areas = ui->stackedWidget->findChildren<QScrollArea*>();
        for(auto&& area : areas)
            area->setStyleSheet("");


        QBrush qb;
        qb.setColor(Qt::white);

        const auto graphs = ui->stackedWidget->findChildren<QChartView*>();
        for(auto&& graph : graphs) {
            graph->setStyleSheet("");
            graph->chart()->setBackgroundBrush((const QBrush)qb);
            graph->chart()->axisY()->setLabelsColor(Qt::black);
            graph->chart()->legend()->setLabelColor(Qt::black);
        }
        ui->graphicsView_vueDet->chart()->axisX()->setLabelsColor(Qt::black);

    }

}


void MainWindow::maj_banquiers() {
    if (ui->comboBox_agences_vueDet->is_checked("Agence A")) {
        if (ui->comboBox_banquiers_vueDet->find("Mr. A1") == -1) {
            ui->comboBox_banquiers_vueDet->addCheckItem("Mr. A1", QVariant::TextFormat, Qt::CheckState::Checked);
            ui->comboBox_banquiers_vueDet->addCheckItem("Mr. A2", QVariant::TextFormat, Qt::CheckState::Checked);
        }
    }
    else {
        if (ui->comboBox_banquiers_vueDet->find("Mr. A1") != -1) {
            int i = ui->comboBox_banquiers_vueDet->find("Mr. A1");
            ui->comboBox_banquiers_vueDet->removeItem(i);
            i = ui->comboBox_banquiers_vueDet->find("Mr. A2");
            ui->comboBox_banquiers_vueDet->removeItem(i);
        }
    }


    if (ui->comboBox_agences_vueDet->is_checked("Agence B")) {
        if (ui->comboBox_banquiers_vueDet->find("Mr. B1") == -1) {
            ui->comboBox_banquiers_vueDet->addCheckItem("Mr. B1", QVariant::TextFormat, Qt::CheckState::Checked);
            ui->comboBox_banquiers_vueDet->addCheckItem("Mr. B2", QVariant::TextFormat, Qt::CheckState::Checked);
        }
    }
    else {
        if (ui->comboBox_banquiers_vueDet->find("Mr. B1") != -1) {
            int i = ui->comboBox_banquiers_vueDet->find("Mr. B1");
            ui->comboBox_banquiers_vueDet->removeItem(i);
            i = ui->comboBox_banquiers_vueDet->find("Mr. B2");
            ui->comboBox_banquiers_vueDet->removeItem(i);
        }
    }


    if (ui->comboBox_agences_vueDet->is_checked("Agence C")) {
        if (ui->comboBox_banquiers_vueDet->find("Mr. C1") == -1) {
            ui->comboBox_banquiers_vueDet->addCheckItem("Mr. C1", QVariant::TextFormat, Qt::CheckState::Checked);
            ui->comboBox_banquiers_vueDet->addCheckItem("Mr. C2", QVariant::TextFormat, Qt::CheckState::Checked);
        }
    }
    else {
        if (ui->comboBox_banquiers_vueDet->find("Mr. C1") != -1) {
            int i = ui->comboBox_banquiers_vueDet->find("Mr. C1");
            ui->comboBox_banquiers_vueDet->removeItem(i);
            i = ui->comboBox_banquiers_vueDet->find("Mr. C2");
            ui->comboBox_banquiers_vueDet->removeItem(i);
        }
    }
}


void MainWindow::slot_msgGraphique() {
    if (ui->dateEdit_debut_vueDet->date().daysTo(ui->dateEdit_fin_vueDet->date()) > 7*6) {
        QMessageBox* msgBox = new QMessageBox(this);

        msgBox->setText("En indiquant une durée superieur à 6 semaines le graphique affichera la somme des recettes "
                        "et non l'étalement sur les differentes semaines de la periode.");
        msgBox->setWindowTitle("Information");
        QAbstractButton *ok =
              msgBox->addButton(tr("Ok"), QMessageBox::NoRole);
        ok->hide();
        ok->animateClick(9000);
        msgBox->setModal(false);

        int x = ui->centralwidget->x() + ui->centralwidget->width();
        int y = ui->centralwidget->y() + ui->centralwidget->height();

        msgBox->move(x, y);
        msgBox->show();
    }
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    srand((unsigned)time(0));

    setup();
    //QCheckList *test = new QCheckList(ui->test);
    /*test->addCheckItem("Agence A", QVariant::TextFormat, Qt::CheckState::Unchecked);
    test->addCheckItem("Agence B", QVariant::TextFormat, Qt::CheckState::Unchecked);
    test->addCheckItem("Agence C", QVariant::TextFormat, Qt::CheckState::Unchecked);*/






    connect(ui->comboBox_agences_vueDet, &QCheckList::globalCheckStateChanged, this, &MainWindow::maj_banquiers);








    ui->stackedWidget->setCurrentIndex(VUE_ENS);


    /************************* signaux du main ***************************************/
    connect(ui->comboBox_donnee, &QComboBox::currentTextChanged, this, &MainWindow::edit_gbar);
    connect(ui->comboBox_agence, &QComboBox::currentTextChanged, this, &MainWindow::edit_gbar);
    connect(ui->dateEdit_debut, &QDateTimeEdit::dateChanged, this, &MainWindow::edit_gbar);
    connect(ui->dateEdit_fin, &QDateTimeEdit::dateChanged, this, &MainWindow::edit_gbar);
    connect(ui->print_button, &QPushButton::released, this, &MainWindow::slot_msgChoixPrintEns);

    connect(ui->pushButton_custom, &QPushButton::released, this, &MainWindow::go_custom);
    connect(ui->pushButton_reglage, &QPushButton::released, this, &MainWindow::go_reglage);
    connect(ui->pushButton_vueDet, &QPushButton::released, this, &MainWindow::go_vueDet);

    ui->graphicsView->installEventFilter(this);
    ui->graphicsView_2->installEventFilter(this);
    ui->graphicsView_3->installEventFilter(this);
    QObject::connect(this, SIGNAL(pressGraphicBancaire()), this, SLOT(zoomGraphicBancaire()));
    QObject::connect(this, SIGNAL(pressGraphicAssurance()), this, SLOT(zoomGraphicAssurance()));
    QObject::connect(this, SIGNAL(pressGraphicBoursier()), this, SLOT(zoomGraphicBoursier()));
    ui->graphicsView_zoom->installEventFilter(this);
    QObject::connect(this, SIGNAL(pressGraphicZoom()), this, SLOT(go_main()));


    /************************* signaux de custom **************************************/
    connect(ui->checkBox_nuit_custom, &QCheckBox::stateChanged, this, &MainWindow::applyTheme);
    connect(ui->pushButton_vueEns_custom, &QPushButton::released, this, &MainWindow::go_main);
    connect(ui->pushButton_reglage_custom, &QPushButton::released, this, &MainWindow::go_reglage);
    connect(ui->pushButton_vueDet_custom, &QPushButton::released, this, &MainWindow::go_vueDet);

    auto checksBox = ui->groupBox_checks_custom->findChildren<QCheckBox*>();
    for(auto&& check : checksBox)
        connect(check, &QCheckBox::stateChanged, this, &MainWindow::enable_btnAppliquer);

    auto keysBox = ui->groupBox_keys_custom->findChildren<QKeySequenceEdit*>();
    for(auto&& key : keysBox)
        connect(key, &QKeySequenceEdit::keySequenceChanged, this, &MainWindow::enable_btnAppliquer);

    connect(ui->pushButton_appliquer_custom, &QPushButton::released, this, &MainWindow::edit_user_settings_defaut);



    /************************* signaux de reglage **************************************/
    connect(ui->pushButton_vueEns_reglage, &QPushButton::released, this, &MainWindow::go_main);
    connect(ui->pushButton_custom_reglage, &QPushButton::released, this, &MainWindow::go_custom);
    connect(ui->pushButton_vueDet_reglage, &QPushButton::released, this, &MainWindow::go_vueDet);

    connect(ui->pushButton_appliquer_reglage, &QPushButton::released, this, &MainWindow::edit_user_settings_defaut);

    auto radiosBox = ui->groupBox_radios_reglage->findChildren<QRadioButton*>();
    for(auto&& radio : radiosBox)
        connect(radio, &QRadioButton::clicked, this, &MainWindow::enable_btnAppliquer);

    auto combosBox = ui->groupBox_combos_reglage->findChildren<QComboBox*>();
    for(auto&& combo : combosBox)
        connect(combo, &QComboBox::currentTextChanged, this, &MainWindow::enable_btnAppliquer);

    connect(ui->spinBox_choixPeriode_reglage, SIGNAL(valueChanged(int)), this, SLOT(enable_btnAppliquer_2(int)));



    /************************* signaux de zoom **************************************/
    connect(ui->pushButton_retour_zoom, &QPushButton::released, this, &MainWindow::go_main);


    /************************* signaux de vueDet **************************************/
    connect(ui->pushButton_vueEns_vueDet, &QPushButton::released, this, &MainWindow::go_main);
    connect(ui->pushButton_custom_vueDet, &QPushButton::released, this, &MainWindow::go_custom);
    connect(ui->pushButton_reglage_vueDet, &QPushButton::released, this, &MainWindow::go_reglage);
    connect(ui->dateEdit_debut_vueDet, &QDateEdit::dateChanged, this, &MainWindow::slot_msgGraphique);
    connect(ui->dateEdit_fin_vueDet, &QDateEdit::dateChanged, this, &MainWindow::slot_msgGraphique);

    /****************************/
    const auto checkBoxList = ui->tab_C->findChildren<QCheckBox*>();
    for(auto&& singleBox : checkBoxList)
        connect(singleBox, &QCheckBox::stateChanged, this, &MainWindow::hide_tabs_from_C);
    const auto checkBoxList2 = ui->tab_F->findChildren<QCheckBox*>();
    for(auto&& singleBox : checkBoxList2)
        connect(singleBox, &QCheckBox::stateChanged, this, &MainWindow::hide_tabs_from_F);
    const auto checkBoxList3 = ui->tab_V->findChildren<QCheckBox*>();
    for(auto&& singleBox : checkBoxList3)
        connect(singleBox, &QCheckBox::stateChanged, this, &MainWindow::hide_tabs_from_V);


    //connect(ui->pushButton_decocherB_vueDet, &QPushButton::released, this, &MainWindow::tout_decocher);
    connect(ui->pushButton_decocherV_vueDet, &QPushButton::released, this, &MainWindow::tout_decocher);
    connect(ui->pushButton_decocherF_vueDet, &QPushButton::released, this, &MainWindow::tout_decocher);
    connect(ui->pushButton_decocherC_vueDet, &QPushButton::released, this, &MainWindow::tout_decocher);

    //ui->pushButton_decocherB_vueDet->setEnabled(false);
    ui->pushButton_decocherV_vueDet->setEnabled(false);
    ui->pushButton_decocherF_vueDet->setEnabled(false);
    ui->pushButton_decocherC_vueDet->setEnabled(true);

    ui->tabWidget_options_vueDet->setTabEnabled(0, true);
    ui->tabWidget_options_vueDet->setTabEnabled(1, false);
    ui->tabWidget_options_vueDet->setTabEnabled(2, false);

    connect(ui->tabWidget_options_vueDet->tabBar(), SIGNAL(tabBarClicked(int)), this, SLOT(slot_msgAutoKill(int)));

    connect(ui->pushButton_appliquer_vueDet, &QPushButton::released, this, &MainWindow::edit_gbar_det);


    const auto checksVueDet = ui->page_vueDet->findChildren<QCheckBox*>();
    for(auto&& check : checksVueDet)
        connect(check, &QCheckBox::stateChanged, this, &MainWindow::enable_btnAppliquer);
    const auto combosVueDet = ui->page_vueDet->findChildren<QComboBox*>();
    for(auto&& combo : combosVueDet)
        connect(combo, &QComboBox::currentTextChanged, this, &MainWindow::enable_btnAppliquer);
    const auto datesVueDet = ui->page_vueDet->findChildren<QDateEdit*>();
    for(auto&& date : datesVueDet)
        connect(date, &QDateEdit::dateChanged, this, &MainWindow::enable_btnAppliquer);






}

MainWindow::~MainWindow()
{
    delete ui;
}

