#include "../headers/mainwindow.h"
#include "../ui_mainwindow.h"


void MainWindow::hide_tabs_from_C() {

    bool allNotChecked = true;
    const auto tab1 = ui->tab_C->findChildren<QCheckBox*>();

    for(auto&& box : tab1)
        if (box->isChecked())
            allNotChecked = false;
    if (allNotChecked) {
        ui->pushButton_decocherC_vueDet->setEnabled(false);
        ui->tabWidget_options_vueDet->setTabEnabled(1, true);
        ui->tabWidget_options_vueDet->setTabEnabled(2, true);

    }
    else {
        ui->pushButton_decocherC_vueDet->setEnabled(true);
        ui->tabWidget_options_vueDet->setTabEnabled(1, false);
        ui->tabWidget_options_vueDet->setTabEnabled(2, false);ui->tabWidget_options_vueDet->setTabEnabled(3, false);
    }
}


void MainWindow::hide_tabs_from_V() {

    bool allNotChecked = true;
    const auto tab1 = ui->tab_V->findChildren<QCheckBox*>();

    for(auto&& box : tab1)
        if (box->isChecked())
            allNotChecked = false;
    if (allNotChecked) {
        ui->pushButton_decocherV_vueDet->setEnabled(false);
        ui->tabWidget_options_vueDet->setTabEnabled(0, true);
        ui->tabWidget_options_vueDet->setTabEnabled(1, true);

    }
    else {
        ui->pushButton_decocherV_vueDet->setEnabled(true);
        ui->tabWidget_options_vueDet->setTabEnabled(0, false);
        ui->tabWidget_options_vueDet->setTabEnabled(1, false);

    }
}

void MainWindow::hide_tabs_from_F() {

    bool allNotChecked = true;
    const auto tab1 = ui->tab_F->findChildren<QCheckBox*>();

    for(auto&& box : tab1)
        if (box->isChecked())
            allNotChecked = false;
    if (allNotChecked) {
        ui->pushButton_decocherF_vueDet->setEnabled(false);
        ui->tabWidget_options_vueDet->setTabEnabled(0, true);
        ui->tabWidget_options_vueDet->setTabEnabled(2, true);

    }
    else {
        ui->pushButton_decocherF_vueDet->setEnabled(true);
        ui->tabWidget_options_vueDet->setTabEnabled(0, false);
        ui->tabWidget_options_vueDet->setTabEnabled(2, false);

    }
}


void MainWindow::tout_decocher() {
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());

    if (buttonSender->objectName() == "pushButton_decocherC_vueDet") {
        const auto tab = ui->tab_C->findChildren<QCheckBox*>();
        for(auto&& box : tab)
            box->setChecked(false);
    }
    else if (buttonSender->objectName() == "pushButton_decocherF_vueDet") {
        const auto tab = ui->tab_F->findChildren<QCheckBox*>();
        for(auto&& box : tab)
            box->setChecked(false);
    }
    else {
        const auto tab = ui->tab_V->findChildren<QCheckBox*>();
        for(auto&& box : tab)
            box->setChecked(false);
    }

}

void MainWindow::slot_msgAutoKill(int a) {
    /*QDateEdit* buttonSender = qobject_cast<QDateEdit*>(sender());
    buttonSender->hide();

    QMessageBox::information(this,"x","e",QMessageBox::Ok);*/


    if (!ui->tabWidget_options_vueDet->isTabEnabled(a)) {
        QMessageBox* msgBox = new QMessageBox(this);

        msgBox->setText("Veuillez décocher tous les elements de l'onglet actuel pour acceder à un autre onglet");
        msgBox->setWindowTitle("Indication");
        QAbstractButton *ok =
              msgBox->addButton(tr("Ok"), QMessageBox::NoRole);
        ok->hide();
        ok->animateClick(5000);
        msgBox->setModal(false);

        int x = ui->centralwidget->x() + ui->centralwidget->width();
        int y = ui->centralwidget->y() + ui->centralwidget->height();

        msgBox->move(x, y);
        msgBox->show();
    }
}
