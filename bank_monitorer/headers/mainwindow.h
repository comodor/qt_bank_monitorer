#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

enum index_page : int {
    VUE_ENS,
    DETAILS,
    CUSTOM,
    REGLAGE,
    VUE_DET,
    VUE_ZOOM
};


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void print_pdf(QString canal);
    void setup();
    void setup_reglage_defaut();
    bool eventFilter(QObject *watched, QEvent *event);

    void hide_tabs_from_C();
    void hide_tabs_from_V();
    void hide_tabs_from_F();
    void tout_decocher();
    std::vector<QString> get_data_det();

    void edit_gbar_det();


private slots:
  void slot_msgAutoKill(int a);
  void slot_msgChoixPrintEns();
  void edit_gbar();
  void go_main();
  void go_custom();
  void go_reglage();
  void go_vueDet();

  void enable_btnAppliquer();
  void enable_btnAppliquer_2(int i);
  void zoomGraphicBancaire();
  void zoomGraphicAssurance();
  void zoomGraphicBoursier();

  void edit_user_settings_defaut();
  void raccourcis();

  void maj_banquiers();

  void applyTheme();

  void slot_msgGraphique();



signals:
  void pressGraphicBancaire();
  void pressGraphicAssurance();
  void pressGraphicBoursier();
  void pressGraphicZoom();



private:
    Ui::MainWindow *ui;
    std::vector<QShortcut*> m_keys;
};
#endif // MAINWINDOW_H
