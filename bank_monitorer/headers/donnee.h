#ifndef DONNEE_H
#define DONNEE_H

#include "mainwindow.h"
#include "../ui_mainwindow.h"

std::vector<QString> get_familles_bancaire() {
    std::vector<QString> f;
    f.push_back("Prêts");
    f.push_back("Comptes");
    f.push_back("Chequiers");
    f.push_back("Cartes");
    return f;
}

std::vector<QString> get_familles_assurance() {
    std::vector<QString> f;
    f.push_back("Assurance");
    return f;
}


std::vector<QString> get_familles_boursier() {
    std::vector<QString> f;
    f.push_back("Action");
    return f;
}

std::vector<QString> MainWindow::get_data_det() {
    std::vector<QString> f;
    auto tab = ui->tab_C->findChildren<QCheckBox*>();

    if (ui->tabWidget_options_vueDet->currentIndex() == 1)
        tab = ui->tab_F->findChildren<QCheckBox*>();
    else if (ui->tabWidget_options_vueDet->currentIndex() == 2)
        tab = ui->tab_V->findChildren<QCheckBox*>();
    else
        tab = ui->tab_C->findChildren<QCheckBox*>();

    for(auto&& box : tab)
        if (box->isChecked())
            f.push_back(box->text());

    return f;
}

#endif // DONNEE_H
