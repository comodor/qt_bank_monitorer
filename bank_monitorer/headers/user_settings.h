#ifndef USER_SETTINGS_H
#define USER_SETTINGS_H

#include "graphics.h"
#include "mainwindow.h"
#include "../ui_mainwindow.h"
#include <fstream>

enum type_donnee {
    VOLUME,
    RECETTE,
    POURCENTAGE
};

enum type_donnee type_donnee_defaut = RECETTE;
QString agence_defaut = "Agence B";
int date_debut_defaut = 5;

enum type_donnee get_default_type_donnee() {

    std::ifstream fd("user_settings");
    std::string tmp_line;
    int recup;

    if (!fd)
        throw std::runtime_error("ouverture fichier impossible");

    std::getline(fd, tmp_line);

    fd >> recup;

    if (recup == 0)
        return VOLUME;
    else if (recup == 1)
        return RECETTE;
    else
        return POURCENTAGE;
}

QString get_default_agence() {

    std::ifstream fd("user_settings");
    std::string tmp_line;
    QString agence;

    if (!fd)
        throw std::runtime_error("ouverture fichier impossible");

    std::getline(fd, tmp_line);
    agence = QString::fromStdString(tmp_line);
    return agence;
}


int get_default_date_debut() {
    std::ifstream fd("user_settings");
    std::string tmp_line;
    int result;

    if (!fd)
        throw std::runtime_error("ouverture fichier impossible");

    std::getline(fd, tmp_line);
    std::getline(fd, tmp_line);

    fd >> result;
    return result;
}

bool get_default_mode_nuit() {
    std::ifstream fd("user_settings");
    std::string tmp_line;
    int result;

    if (!fd)
        throw std::runtime_error("ouverture fichier impossible");

    std::getline(fd, tmp_line);
    std::getline(fd, tmp_line);
    std::getline(fd, tmp_line);

    fd >> result;
    return result;
}

QString get_default_devise() {
    std::ifstream fd("user_settings");
    std::string tmp_line;
    int result;

    if (!fd)
        throw std::runtime_error("ouverture fichier impossible");

    std::getline(fd, tmp_line);
    std::getline(fd, tmp_line);
    std::getline(fd, tmp_line);
    std::getline(fd, tmp_line);

    fd >> result;
    QString devise = "";
    if (result == 0)
        devise = "euros";
    else if (result == 1)
        devise = "dollars";
    else
        devise = "livres";
    return devise;
}




std::string get_default_key(int n) {
    std::ifstream fd("user_settings");
    std::string tmp_line;

    if (!fd)
        throw std::runtime_error("ouverture fichier impossible");

    for (int i = 0; i < n; i++)
        std::getline(fd, tmp_line);

    return tmp_line;
}



void set_default_type_donnee(QComboBox* comboBox) {
    std::ifstream fd_reg("user_settings");
    std::string l1, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15;

    std::getline(fd_reg, l1);
    std::getline(fd_reg, l3);
    std::getline(fd_reg, l3);
    std::getline(fd_reg, l4);
    std::getline(fd_reg, l5);
    std::getline(fd_reg, l6);
    std::getline(fd_reg, l7);
    std::getline(fd_reg, l8);
    std::getline(fd_reg, l9);
    std::getline(fd_reg, l10);
    std::getline(fd_reg, l11);
    std::getline(fd_reg, l12);
    std::getline(fd_reg, l13);
    std::getline(fd_reg, l14);
    std::getline(fd_reg, l15);


    fd_reg.close();

    std::ofstream fd_new("user_settings");

    fd_new << l1 << "\n";
    fd_new << std::to_string(comboBox->currentIndex()) << "\n";
    fd_new << l3 << "\n";
    fd_new << l4 << "\n";
    fd_new << l5 << "\n";
    fd_new << l6 << "\n";
    fd_new << l7 << "\n";
    fd_new << l8 << "\n";
    fd_new << l9 << "\n";
    fd_new << l10 << "\n";
    fd_new << l11 << "\n";
    fd_new << l12 << "\n";
    fd_new << l13 << "\n";
    fd_new << l14 << "\n";
    fd_new << l15 << "\n";

    fd_new.close();
}


void set_default_agence(QComboBox* comboBox) {
    std::ifstream fd_reg("user_settings");
    std::string l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15;

    std::getline(fd_reg, l2);
    std::getline(fd_reg, l2);
    std::getline(fd_reg, l3);
    std::getline(fd_reg, l4);
    std::getline(fd_reg, l5);
    std::getline(fd_reg, l6);
    std::getline(fd_reg, l7);
    std::getline(fd_reg, l8);
    std::getline(fd_reg, l9);
    std::getline(fd_reg, l10);
    std::getline(fd_reg, l11);
    std::getline(fd_reg, l12);
    std::getline(fd_reg, l13);
    std::getline(fd_reg, l14);
    std::getline(fd_reg, l15);

    fd_reg.close();

    std::ofstream fd_new("user_settings");

    fd_new << comboBox->currentText().toUtf8().constData() << "\n";
    fd_new << l2 << "\n";
    fd_new << l3 << "\n";
    fd_new << l4 << "\n";
    fd_new << l5 << "\n";
    fd_new << l6 << "\n";
    fd_new << l7 << "\n";
    fd_new << l8 << "\n";
    fd_new << l9 << "\n";
    fd_new << l10 << "\n";
    fd_new << l11 << "\n";
    fd_new << l12 << "\n";
    fd_new << l13 << "\n";
    fd_new << l14 << "\n";
    fd_new << l15 << "\n";


    fd_new.close();
}

void set_default_date_debut(QSpinBox* spinbox) {
    std::ifstream fd_reg("user_settings");
    std::string l1, l2, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15;

    std::getline(fd_reg, l1);
    std::getline(fd_reg, l2);
    std::getline(fd_reg, l4);
    std::getline(fd_reg, l4);
    std::getline(fd_reg, l5);
    std::getline(fd_reg, l6);
    std::getline(fd_reg, l7);
    std::getline(fd_reg, l8);
    std::getline(fd_reg, l9);
    std::getline(fd_reg, l10);
    std::getline(fd_reg, l11);
    std::getline(fd_reg, l12);
    std::getline(fd_reg, l13);
    std::getline(fd_reg, l14);
    std::getline(fd_reg, l15);

    fd_reg.close();

    std::ofstream fd_new("user_settings");

    fd_new << l1 << "\n";
    fd_new << l2 << "\n";
    fd_new << std::to_string(spinbox->value()) << "\n";
    fd_new << l4 << "\n";
    fd_new << l5 << "\n";
    fd_new << l6 << "\n";
    fd_new << l7 << "\n";
    fd_new << l8 << "\n";
    fd_new << l9 << "\n";
    fd_new << l10 << "\n";
    fd_new << l11 << "\n";
    fd_new << l12 << "\n";
    fd_new << l13 << "\n";
    fd_new << l14 << "\n";
    fd_new << l15 << "\n";

    fd_new.close();
}

void set_default_mode_nuit(QCheckBox* btn) {
    std::ifstream fd_reg("user_settings");
    std::string l1, l2, l3, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15;

    std::getline(fd_reg, l1);
    std::getline(fd_reg, l2);
    std::getline(fd_reg, l3);
    std::getline(fd_reg, l5);
    std::getline(fd_reg, l5);
    std::getline(fd_reg, l6);
    std::getline(fd_reg, l7);
    std::getline(fd_reg, l8);
    std::getline(fd_reg, l9);
    std::getline(fd_reg, l10);
    std::getline(fd_reg, l11);
    std::getline(fd_reg, l12);
    std::getline(fd_reg, l13);
    std::getline(fd_reg, l14);
    std::getline(fd_reg, l15);

    fd_reg.close();

    std::ofstream fd_new("user_settings");

    fd_new << l1 << "\n";
    fd_new << l2 << "\n";
    fd_new << l3 << "\n";
    if (btn->isChecked())
        fd_new << "1\n";
    else
        fd_new << "0\n";
    fd_new << l5 << "\n";
    fd_new << l6 << "\n";
    fd_new << l7 << "\n";
    fd_new << l8 << "\n";
    fd_new << l9 << "\n";
    fd_new << l10 << "\n";
    fd_new << l11 << "\n";
    fd_new << l12 << "\n";
    fd_new << l13 << "\n";
    fd_new << l14 << "\n";
    fd_new << l15 << "\n";

    fd_new.close();
}


void set_default_devise(QRadioButton* btn1, QRadioButton* btn2, QRadioButton* btn3) {
    std::ifstream fd_reg("user_settings");
    std::string l1, l2, l3, l4, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15;

    std::getline(fd_reg, l1);
    std::getline(fd_reg, l2);
    std::getline(fd_reg, l3);
    std::getline(fd_reg, l4);
    std::getline(fd_reg, l6);
    std::getline(fd_reg, l6);
    std::getline(fd_reg, l7);
    std::getline(fd_reg, l8);
    std::getline(fd_reg, l9);
    std::getline(fd_reg, l10);
    std::getline(fd_reg, l11);
    std::getline(fd_reg, l12);
    std::getline(fd_reg, l13);
    std::getline(fd_reg, l14);
    std::getline(fd_reg, l15);


    fd_reg.close();

    std::ofstream fd_new("user_settings");

    fd_new << l1 << "\n";
    fd_new << l2 << "\n";
    fd_new << l3 << "\n";
    fd_new << l4 << "\n";
    if (btn1->isChecked())
        fd_new << "0\n";
    if (btn2->isChecked())
        fd_new << "1\n";
    if (btn3->isChecked())
        fd_new << "2\n";
    fd_new << l6 << "\n";
    fd_new << l7 << "\n";
    fd_new << l8 << "\n";
    fd_new << l9 << "\n";
    fd_new << l10 << "\n";
    fd_new << l11 << "\n";
    fd_new << l12 << "\n";
    fd_new << l13 << "\n";
    fd_new << l14 << "\n";
    fd_new << l15 << "\n";

    fd_new.close();
}


void set_default_keys(QKeySequenceEdit* keyN, QKeySequenceEdit* keyVE, QKeySequenceEdit* keyVD,
                      QKeySequenceEdit* keyC, QKeySequenceEdit* keyR, QKeySequenceEdit* keyRt,
                      QKeySequenceEdit* keyZ1, QKeySequenceEdit* keyZ2, QKeySequenceEdit* keyZ3,
                      QKeySequenceEdit* keyZD) {

    std::ifstream fd_reg("user_settings");
    std::string l1, l2, l3, l4, l5;

    std::getline(fd_reg, l1);
    std::getline(fd_reg, l2);
    std::getline(fd_reg, l3);
    std::getline(fd_reg, l4);
    std::getline(fd_reg, l5);
    fd_reg.close();

    std::ofstream fd_new("user_settings");
    fd_new << l1 << "\n";
    fd_new << l2 << "\n";
    fd_new << l3 << "\n";
    fd_new << l4 << "\n";
    fd_new << l5 << "\n";

    fd_new << keyN->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyVE->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyVD->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyC->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyR->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyRt->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyZ1->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyZ2->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyZ3->keySequence().toString().toUtf8().constData() << "\n";
    fd_new << keyZD->keySequence().toString().toUtf8().constData() << "\n";

    fd_new.close();
}

#endif // USER_SETTINGS_H
