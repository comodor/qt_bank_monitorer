#ifndef GRAPHICS_H
#define GRAPHICS_H



#include "user_settings.h"
#include "donnee.h"

#include <QLineSeries>
#include <QChart>
#include <QAreaSeries>
#include <QBarSet>
#include <QBarSeries>
#include <ctime>
#include <QPrinter>

using namespace QtCharts;





QString m_last_pdf_create;
QString m_last_path_pdf_create;



/**
 * @brief à partir d'un nombre le convertie en chaine de caractere et insere un '0' avant si le
 *  nombre est à 1 chiffre.
 */
QString add_zero(int number) {
    QString n;
    if (number >= 0 && number < 10)
        n = "0" + QString::number(number);
    else
        n = QString::number(number);
    return n;

}

QChart* create_gbar(std::vector<QString> gbar_familles, QDateEdit* debut, QDateEdit* fin) {

    std::vector<QBarSet*> colonnes;
    for (int i = 0; i < (int)gbar_familles.size(); i++)
    {
        QBarSet *set = new QBarSet(gbar_familles[i]);
        if (get_default_type_donnee() == POURCENTAGE)
            *set << ((rand()%(100- 25))+15)%100;
        else if (get_default_type_donnee() == VOLUME)
            *set << ((rand()%(1000 - 100))+90)%1000;
        else
            *set << ((rand()%(10000 - 1000))+900)%10000;
        colonnes.push_back(set);
    }

    QBarSeries *series = new QBarSeries();
    for (int i = 0; i < (int)gbar_familles.size(); i++)
        series->append(colonnes[i]);

    QChart *chart = new QChart();
    chart->addSeries(series);

    QString stype;
    QString periode;
    if (get_default_type_donnee() == POURCENTAGE)
        stype = "données en pourcentage du ";
    else if (get_default_type_donnee() == VOLUME)
        stype = "données en quantité du ";
    else
        stype = "données en " + get_default_devise() + "du ";

    periode = add_zero(debut->date().day()) + "/"
        + add_zero(debut->date().month()) + "/"
        + add_zero(debut->date().year())
        + " au "
        + add_zero(fin->date().day()) + "/"
        + add_zero(fin->date().month()) + "/"
        + add_zero(fin->date().year());

    chart->setTitle(stype + periode);
    chart->setAnimationOptions(QChart::SeriesAnimations);

    QValueAxis *axisY = new QValueAxis();
    if (get_default_type_donnee() == POURCENTAGE)
        axisY->setRange(0, 100);
    else if (get_default_type_donnee() == VOLUME)
        axisY->setRange(0, 1000);
    else
        axisY->setRange(0, 10000);

    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    if (get_default_mode_nuit()) {
        QBrush qb;
        qb.setColor(Qt::black);
        chart->setBackgroundBrush((const QBrush)qb);
        chart->axisY()->setLabelsColor(Qt::white);
        chart->legend()->setLabelColor(Qt::white);
    }

    return chart;
}



QChart* get_edit_gbar(QComboBox* box_type, std::vector<QString> gbar_familles, QDateEdit* debut, QDateEdit* fin) {
    QString stype;
    QString periode;


    if (box_type->currentText() == "Pourcentage")
        stype = "données en pourcentage du ";
    else if (box_type->currentText() == "Volume")
        stype = "données en quantité du ";
    else
        stype = "données en " + get_default_devise() + " du ";

    periode = add_zero(debut->date().day()) + "/"
        + add_zero(debut->date().month()) + "/"
        + add_zero(debut->date().year())
        + " au "
        + add_zero(fin->date().day()) + "/"
        + add_zero(fin->date().month()) + "/"
        + add_zero(fin->date().year());

    QChart *chart = new QChart();
    chart->setTitle(stype + periode);

    //chart->removeAxis(chart->axisY());
    QValueAxis *axisY = new QValueAxis();
    if (box_type->currentText() == "Pourcentage")
        axisY->setRange(0, 100);
    else if (box_type->currentText() == "Volume")
        axisY->setRange(0, 1000);
    else
        axisY->setRange(0, 10000);
    chart->addAxis(axisY, Qt::AlignLeft);

    std::vector<QBarSet*> colonnes;
    for (int i = 0; i < (int)gbar_familles.size(); i++)
    {
        QBarSet *set = new QBarSet(gbar_familles[i]);
        if (box_type->currentText() == "Pourcentage")
            *set << ((rand()%(100- 25))+15)%100;
        else if (box_type->currentText() == "Volume")
            *set << ((rand()%(1000 - 100))+90)%1000;
        else
            *set << ((rand()%(10000 - 1000))+900)%10000;
        colonnes.push_back(set);
    }

    //chart->removeAllSeries();
    QBarSeries *series = new QBarSeries();
    for (int i = 0; i < (int)gbar_familles.size(); i++)
        series->append(colonnes[i]);



    chart->addSeries(series);
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
    chart->setAnimationOptions(QChart::SeriesAnimations);

    if (get_default_mode_nuit()) {
        QBrush qb;
        qb.setColor(Qt::black);
        chart->setBackgroundBrush((const QBrush)qb);
        chart->axisY()->setLabelsColor(Qt::white);
        chart->legend()->setLabelColor(Qt::white);
    }

    return chart;
}

void MainWindow::edit_gbar() {
    QChart* chart = get_edit_gbar(ui->comboBox_donnee, get_familles_bancaire(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView->setChart(chart);
    QChart* chart2 = get_edit_gbar(ui->comboBox_donnee, get_familles_assurance(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_2->setChart(chart2);
    QChart* chart3 = get_edit_gbar(ui->comboBox_donnee, get_familles_boursier(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_3->setChart(chart3);
}




QChart* create_gbar_det(std::vector<QString> gbar_familles,
                        QCheckList* agences, QCheckList* banquiers,
                        QDateEdit* debut, QDateEdit* fin) {


    std::vector<QString> data;

    for (int i = 0; i < (int)gbar_familles.size(); i++) {
        std::vector<QString> myvector = banquiers->get_checked();
        std::vector<QString>::iterator it;
        std::vector<QString>::iterator it2;

        it = find(myvector.begin(), myvector.end(), "Mr. A1");
        it2 = find(myvector.begin(), myvector.end(), "Mr. A2");

        if (it != myvector.end() && it2 != myvector.end())
            data.push_back(gbar_familles[i] + " - agence A");
        else {
            if (it != myvector.end())
                data.push_back(gbar_familles[i] + " - Mr. A1");
            if (it2 != myvector.end())
                data.push_back(gbar_familles[i] + " - Mr. A2");
        }


        it = find(myvector.begin(), myvector.end(), "Mr. B1");
        it2 = find(myvector.begin(), myvector.end(), "Mr. B2");

        if (it != myvector.end() && it2 != myvector.end())
            data.push_back(gbar_familles[i] + " - agence B");
        else {
            if (it != myvector.end())
                data.push_back(gbar_familles[i] + " - Mr. B1");
            if (it2 != myvector.end())
                data.push_back(gbar_familles[i] + " - Mr. B2");
        }


        it = find(myvector.begin(), myvector.end(), "Mr. C1");
        it2 = find(myvector.begin(), myvector.end(), "Mr. C2");

        if (it != myvector.end() && it2 != myvector.end())
            data.push_back(gbar_familles[i] + " - agence C");
        else {
            if (it != myvector.end())
                data.push_back(gbar_familles[i] + " - Mr. C1");
            if (it2 != myvector.end())
                data.push_back(gbar_familles[i] + " - Mr. C2");
        }
    }





    QStringList categories;
    int cpt = 1;

    int nb_semaine = debut->date().daysTo(fin->date()) / 7;
    if ((debut->date().daysTo(fin->date()) / 7) > 6)
        categories << debut->date().toString("d MMM yyyy") + " - " + fin->date().toString("q MMM yyyy");

    else {
        QDate d;
        d.setDate(debut->date().year(), debut->date().month(), debut->date().day());
        int jour = d.dayOfWeek() - 1;
        d = d.addDays(-jour);

        for (int i = 0; i < nb_semaine; i++) {
            categories << "semaine du " + d.toString("d MMM yyyy");
            d = d.addDays(7);
            cpt++;
        }
    }

    QBarSeries *series = new QBarSeries();
    for (int i = 0; i < (int)data.size(); i++) {
        QBarSet *set = new QBarSet(data[i]);

        for (int j = 0; j < cpt; j++) {
            *set << ((rand()%(100- 25))+15)%100;
        }

        series->append(set);
    }



    QChart *chart = new QChart();
    chart->addSeries(series);

    chart->setAnimationOptions(QChart::SeriesAnimations);




    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis();
    axisY->setRange(0,100);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    if (get_default_mode_nuit()) {
        QBrush qb;
        qb.setColor(Qt::black);
        chart->setBackgroundBrush((const QBrush)qb);
        chart->axisY()->setLabelsColor(Qt::white);
        chart->legend()->setLabelColor(Qt::white);
    }
    return chart;
}





/**
 * @brief à partir de l'objet graphique et du tableau d'info du graphique, créer un pdf insere les infos
 *  du graphique, le header... puis l'enregistre avec comme nom la date actuelle avec l'heure
 */
void print_graph(QtCharts::QChartView* graphicsView, std::vector<QString> infos) {
    time_t date = time(0);
    tm *ltm = localtime(&date);

    // Creation fichier
    QPrinter printer(QPrinter::HighResolution);
    printer.setPageSize(QPrinter::A4);
    QString path = "../bank_monitorer/output/";

    QString format_pdf_output = "graphique_"
        + add_zero(ltm->tm_mday) + "."
        + add_zero(1 + ltm->tm_mon) + "."
        + add_zero(1900 + ltm->tm_year) + "_"
        + add_zero(ltm->tm_hour) + "h"
        + add_zero(ltm->tm_min) + "m"
        + add_zero(1 + ltm->tm_sec) + "s.pdf";
    printer.setOutputFileName(path + format_pdf_output);
    QPainter painter(&printer);
    m_last_pdf_create = format_pdf_output;
    m_last_path_pdf_create = path;

    // Ajout header
    QImage img("../bank_monitorer/ressources/graph_header.png");
    Q_ASSERT(!img.isNull());
    painter.drawImage(QRect(-500, -1000, 10000, 3000), img);

    // Ajout footer
    QString footerTextList = "créer le " + QDateTime::currentDateTime().toString();
    painter.drawText(printer.width() - 2500, printer.height() - 200, footerTextList);

    // Ajout infos du graph
    painter.setFont(QFont("Verdana",14));
    QString info_graph;
    info_graph = "agence(s): " + infos[0];    painter.drawText(900, 3000, info_graph);
    info_graph = "periode: " + infos[1];    painter.drawText(900, 3250, info_graph);
    info_graph = "type des données: " + infos[2];    painter.drawText(900, 3500, info_graph);
    info_graph = "canal(aux): " + infos[3];    painter.drawText(900, 3750, info_graph);
    info_graph = "famille(s): " + infos[4];    painter.drawText(900, 4000, info_graph);
    info_graph = "produit(s): " + infos[5];    painter.drawText(900, 4250, info_graph);


    // Ajout du graph
    QRect viewport = graphicsView->viewport()->rect();
    graphicsView->render(&painter,
        QRectF(0, printer.height()/2 - (printer.height()*0.05),
               printer.width(), printer.height()/2),
               viewport.adjusted(0, 0, 0, 0));
}


/**
 * @brief recupere toutes les infos d'un graphique à partir des objets de reglage des graphique
 *  puis les mets dans un tableau
 */
std::vector<QString> get_info_print
    (QComboBox* box_agence, QDateEdit* date_debut, QDateEdit* date_fin,QComboBox* box_donnee,
     std::vector<QString> canaux, std::vector<QString> familles,
     std::vector<QString> produits)
{
    std::vector<QString> infos;
    infos.push_back(box_agence->currentText());
    infos.push_back(date_debut->date().toString() + " - " + date_fin->date().toString());
    infos.push_back(box_donnee->currentText());

    QString tmp = "";
    for (int i = 0; i < (int)canaux.size(); i++)
        tmp +=  canaux[i] + " ";
    infos.push_back(tmp);

    tmp = "";
    for (int i = 0; i < (int)familles.size(); i++)
        tmp +=  familles[i] + " ";
    infos.push_back(tmp);

    tmp = "";
    for (int i = 0; i < (int)produits.size(); i++)
        tmp +=  produits[i] + " ";
    infos.push_back(tmp);

    return infos;
}




bool MainWindow::eventFilter(QObject *obj, QEvent *event) {
    QKeyEvent* key = static_cast<QKeyEvent*>(event);
    if ((obj == ui->graphicsView && event->type() == QEvent::MouseButtonPress)
            || ((key->key() == tr(get_default_key(12).c_str()))) ) {
        emit pressGraphicBancaire();
        return true;
    }
    if ((obj == ui->graphicsView_2 && event->type() == QEvent::MouseButtonPress)
            || ((key->key() == tr(get_default_key(13).c_str())))) {
        emit pressGraphicAssurance();
        return true;
    }
    if ((obj == ui->graphicsView_3 && event->type() == QEvent::MouseButtonPress)
            || ((key->key() == tr(get_default_key(14).c_str())))) {
        emit pressGraphicBoursier();
        return true;
    }
    if ((obj == ui->graphicsView_zoom && event->type() == QEvent::MouseButtonPress)
            || ((key->key() == tr(get_default_key(11).c_str())))) {
        emit pressGraphicZoom();
        return true;
    }



    return QObject::eventFilter(obj, event);
}



void MainWindow::zoomGraphicBancaire() {
    QChart* chart = get_edit_gbar(ui->comboBox_donnee, get_familles_bancaire(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_zoom->setChart(chart);
    ui->stackedWidget->setCurrentIndex(VUE_ZOOM);
}

void MainWindow::zoomGraphicAssurance() {
    QChart* chart = get_edit_gbar(ui->comboBox_donnee, get_familles_assurance(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_zoom->setChart(chart);
    ui->stackedWidget->setCurrentIndex(VUE_ZOOM);
}

void MainWindow::zoomGraphicBoursier() {
    QChart* chart = get_edit_gbar(ui->comboBox_donnee, get_familles_boursier(), ui->dateEdit_debut, ui->dateEdit_fin);
    ui->graphicsView_zoom->setChart(chart);
    ui->stackedWidget->setCurrentIndex(VUE_ZOOM);
}





#endif // GRAPHICS_H
